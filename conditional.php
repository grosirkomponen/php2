<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
/* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di PKS Digital School!"
*/

// Code function di sini
function greetings($nama){
    echo "Halo $nama, Selamat Datang di PKS Digital School!";
};

// Hapus komentar untuk menjalankan code!
 greetings("Bagas");
 echo "<br>";
 greetings("Wahyu");
 echo "<br>";
 greetings("Abdul");
 echo "<br>";

echo "<h3>Soal No 2 Reverse String</h3>";
/* 
Soal No 2
Reverse String
Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping 
(for/while/do while).
Function reverseString menerima satu parameter berupa string.
NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

reverseString("abdul");
Output: ludba

*/

// Code function di sini 
function reverse($word){
    $jumkata = strlen($word);
    $data_new = "";
    for ($i=($jumkata - 1); $i>=0; $i--){
        $data_new .= $word[$i];
    }
    return $data_new;
}
function reverseString($word2){
    $string = reverse($word2);
    echo $string ."<br>";
}

// Hapus komentar di bawah ini untuk jalankan Code
 reverseString("abduh");
 reverseString("Digital School");
 reverseString("We Are PKS Digital School Developers");
echo "<br>";

echo "<h3>Soal No 3 Palindrome </h3>";
/* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari 
jawaban no.2!

*/


// Code function di sini
function reverse2($word3){
    $jumkata2 = strlen($word3);
    $data_new2 = "";
    for ($i=($jumkata2 - 1); $i>=0; $i--){
        $data_new2 .= $word3[$i];
    }
    return $data_new2;
}
function palindrome($word4){
    $string2 = reverse($word4);
    $output = strcmp($word4, $string2);
    if ($output !== 0)
        {
            echo "Output : 'false'<br>";
        }
    else
        {
            echo "Output : 'true'<br>";
        }
    
}

// Hapus komentar di bawah ini untuk jalankan code
 palindrome("civic") ; //true
 palindrome("nababan") ; // true
 palindrome("jambaban"); // false
 palindrome("racecar"); // true


echo "<h3>Soal No 4 Tentukan Nilai </h3>";
/*
Soal 4
buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn 
String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika 
parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

// Code function di sini
function tentukan_nilai($angka){
    if ($angka>=85 && $angka <=100){
        $nilai = "Sangat Baik";
    } elseif ($angka>=70 && $angka <85){
        $nilai = "Baik";       
    } elseif ($angka>=60 && $angka <70){
        $nilai = "Cukup";       
    } else {
        $nilai = "Kurang"; 
    }
    return $nilai;
}

// Hapus komentar di bawah ini untuk jalankan code
 echo tentukan_nilai(98); //Sangat Baik
 echo "<br>";
 echo tentukan_nilai(76); //Baik
 echo "<br>";
 echo tentukan_nilai(67); //Cukup
 echo "<br>";
 echo tentukan_nilai(43); //Kurang
 echo "<br>";


?>

</body>

</html>
